let exchangeRates = {}

const base = 'reserved'

function makeButtonReseved(url) {
  const button = document.querySelector(`a[href='${url}']`).parentNode.parentNode.children[2].childNodes[0]
  button.textContent = 'Зайнято'
  button.classList.add("reserved")
}

function makeDetaXHR(url, method) {
  var xhr = new XMLHttpRequest();
  xhr.open(method, url, true);
  xhr.setRequestHeader('x-apikey', '670fa831bd27922673ab25e9');
  xhr.setRequestHeader('Content-Type', 'application/json');
  return xhr
}

var xhr = new XMLHttpRequest();
xhr.open("get", "https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json", true);
xhr.onreadystatechange = function () {
  if (xhr.readyState == 4 && xhr.status == 200) {
    JSON.parse(xhr.responseText).forEach(item => {
      if (item.cc == 'USD') {
        exchangeRates['$'] = item.rate
      } else if (item.cc == 'GBP') {
        exchangeRates['£'] = item.rate
      }
    })
  };
}
xhr.send(null);

const queryXHR = makeDetaXHR(`https://wishlist-5ea9.restdb.io/rest/wishes`, 'get')
queryXHR.onreadystatechange = () => {
  if (queryXHR.readyState == 4 && queryXHR.status == 200 || queryXHR.status == 207 || queryXHR.status == 201) {
    JSON.parse(queryXHR.responseText).forEach(item => {
      if (new Date(JSON.parse(item.end_date)) >= new Date()) {
        makeButtonReseved(item.key)
      } else {
        const delete_xhr = makeDetaXHR(`https://wishlist-5ea9.restdb.io/rest/wishes/${item._id}`, "delete");
        delete_xhr.send(null);
      }
    })
  };
}
queryXHR.send(null);

document.getElementById('wishlist__filter').addEventListener('keyup', function (e) {
  if (e.target.value) {
    const filterValue = parseInt(e.target.value)
    document.querySelectorAll('td:nth-child(2) code').forEach(price => {
      let truePrice = price.textContent.split(' ')[0];
      const cc = price.textContent.split(' ')[1];
      price.parentNode.parentNode.style.display = '';
      truePrice = parseInt(truePrice)
      if (cc != 'грн') truePrice = truePrice * exchangeRates[cc]
      if (truePrice > filterValue && filterValue > 0) {
        price.parentNode.parentNode.style.display = 'none';
      }
    })
  } else {
    document.querySelectorAll('td:nth-child(2) code').forEach(price => {
      price.parentNode.parentNode.style.display = '';
    })
  }
});
document.querySelectorAll('tbody tr').forEach(tr => {
  const button = document.createElement('button')
  button.addEventListener('click', function (e) {
    var modal = document.getElementById("reserve__modal");
    modal.style.display = "block";
    const form = document.querySelector("#reserve__modal form")
    const reserved_message = document.querySelector("#reserve__modal .reserved_message")
    form.style.display = "none";
    reserved_message.style.display = "none";
    const url = tr.childNodes[1].childNodes[0].href
    var xhr = makeDetaXHR(`https://wishlist-5ea9.restdb.io/rest/wishes?q={"key": "${encodeURIComponent(url)}"}`, "get");
    xhr.onreadystatechange = function () {
      if (xhr.readyState == 4 && xhr.status == 200 || xhr.status == 207) {
        let response = JSON.parse(xhr.responseText)
        if (response.length > 0) {
          response = response[0]
          reserved_message.innerHTML = `Дарунок зайнятий користувачем <strong>${response.nickname}</strong> до ${new Date(JSON.parse(response.end_date)).toLocaleString("uk-UA", {'day': 'numeric', 'month': 'numeric', 'year': 'numeric'})}`
          reserved_message.style.display = "";
        } else {
          reserved_message.style.display = "none";
          form.style.display = "";
        };
      };
    }
    reserved_message.innerHTML = `Завантаження...`
    reserved_message.style.display = "";
    xhr.send(null);
    document.getElementById('reserved__button').setAttribute('data-url', url)
  })
  button.textContent = 'Резерв'
  if (tr.children.length == 3) {
    tr.children[2].appendChild(button)

  }
})

document.querySelector(".close").addEventListener('click', function (e) {
  var modal = document.getElementById("reserve__modal");
  modal.style.display = "none";
})
document.getElementById('reserved__button').addEventListener('click', function (e) {
  const url = e.target.getAttribute('data-url')
  var xhr = makeDetaXHR(`https://wishlist-5ea9.restdb.io/rest/wishes`, "post")
  xhr.onreadystatechange = function () {
    if (xhr.readyState == 4 && xhr.status == 200 || xhr.status == 207 || xhr.status == 201) {
      var modal = document.getElementById("reserve__modal");
      modal.style.display = "none";
      makeButtonReseved(url)
    };
  }
  // xhr.setRequestHeader('x-apikey', '670fa831bd27922673ab25e9');
  var date = new Date();
  date.setDate(date.getDate() + parseInt(document.getElementById('reserved__days').value));
  const body = {
      
        "end_date": JSON.stringify(date),
        "key": url,
        "nickname": document.getElementById('reserved__name').value
      
  }
    xhr.send(JSON.stringify(body));
  
})