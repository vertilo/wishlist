---
title: "Бажанка"
description: "Бажанка оновлено **01.01.2025**\n\n**Жирним** підкресленні більш пріоритетні дарунки"
# type: "wishlist"
draft: false
---
# Найкращий подарунок

Зроблений своїми рукми :)\
Ну, або гроші [**сюди**](https://send.monobank.ua/79eJuYwC4h), бо зараз тяжко.

# WWF [^1]
|   |   |   |
---|---:|---:|
[**Сула блакитнонога**](https://gifts.worldwildlife.org/gift-center/gifts/Species-Adoptions/Blue-Footed-Booby) |```25 $```|   |
[**Бджілку**](https://gifts.worldwildlife.org/gift-center/gifts/species-adoptions/honeybee) |```60 $```|   |
[**Рибалочка**](https://gifts.worldwildlife.org/gift-center/gifts/species-adoptions/kingfisher) |```25 $```|   |
[**Вівцебик**](https://gifts.worldwildlife.org/gift-center/gifts/species-adoptions/musk-ox) |```60 $```|   |
[**Варан**](https://gifts.worldwildlife.org/gift-center/gifts/species-adoptions/komodo-dragon) |```25 $```|   |

# Відеоігри

|   |   |   |
---|---:|---:|
[**Super Mario 3D All-Stars**](https://www.amazon.com/s?k=Super+Mario+3D+All-Stars) [^2]|```100 $```|   |
[Pokemon Mystery Dungeon: Rescue Team DX](https://prom.ua/ua/p1589995104-pokemon-mystery-dungeon.html)|```1230 грн```|   |
[The Legend of Zelda: Tears of the Kingdom](https://prom.ua/ua/p1847632431-the-legend-zelda.html)|```2160 грн```|   |
[**Nintendo Switch Lite Yellow**](https://retromagaz.com/product/switch-lite-32gb-yellow-zheltyy-novyy)|```8200 грн```|   |
[**Playdate**](https://play.date/shop/) [^2]|```199 $```|   |

# Одяг та аксесуари

|   |   |   |
---|---:|---:|
[Набір шкарпеток Гриби (42-43 р.)](https://dodosocks.com/shop/mushrooms-set/)|```290 грн```|   |
[Шкарпетки Короткi Асканія Нова box (43-46 р.)](https://griffonsocks.com.ua/product/korotki-askaniya-nova-box/)|```500 грн```|   |
[Шкарпетки Який ти птах? (43-46 р.)](https://griffonsocks.com.ua/product/yakyj-ty-ptah/)|```500 грн```|   |
[**Рюкзак Osprey Arcane Flap Pack**](https://www.gorgany.com/ryukzak-osprey-arcane-flap-pack.html)|```4195 грн```|   |
[**Fair of the Fungi Hoodie (2XL)**](https://www.campmustelid.shop/product/fair-of-the-fungi-hoodie-preorder) [^2]|```70 $```|   |
[**Mushroom Peekaboo Beanie**](https://www.campmustelid.shop/product/mushroom-beanie) [^2]|```35 $```|   |
[**Sea Otter Peekaboo Beanie Hat**](https://www.campmustelid.shop/product/sea-otter-beanie) [^2]|```35 $```|   |



# Книжки та інші друковані видання

|   |   |   |
---|---:|---:|
[Манґа Шлях домогосподаря. Том 3-5](https://malopus.com.ua/manga/manga-shlyah-domogospodarya-tom-3)|```200 грн```|   |
[Ательє чаклунських капелюхів](https://nashaidea.com/product/ateliye-chaklunskih-kapeluhiv-tom1/)|```200 грн```|   |
[Чi “Життя однієї киці”](https://nashaidea.com/product/chi_zhittya_tom1/)|```200 грн```|   |
[Монолог Травниці](https://lantsuta-publishing.com/apothecary1-en)|```350 грн```|   |
[Я рада, що моя мама померла](https://artbooks.ua/ja-rada-shcho-moja-mama-pomerla)|```400 грн```|   |
[Лицарі Двородельфа](https://sumnavivtsa.com/books/833/)|```280 грн```|   |

# Чай і все до нього

|   |   |   |
---|---:|---:|
[Мен Дін Хуан Я](https://teahouse.ua/chaj-men-din-khuan-ia-zhovtyj-num060/)|```500 грн```|   |
[Срібні Голки](https://teahouse.ua/chaj-sribni-golki-bajhao-inchzhen-003/)|```390 грн```|   |
[Жасминові перли дракона](https://teahouse.ua/chaj-zhasminovi-perli-drakona-022/)|```280 грн```|   |
[Тегуаньінь свіжий аромат](https://teahouse.ua/chaj-teguanin-svizhij-aromat-200/)|```160 грн```|   |

# Фото

|   |   |   |
---|---:|---:|
[Картриджі Fujifilm Instax Instant Wide Film упакрвка (2x10 фото)](https://prom.ua/ua/p1519303517-kartridzhi-fujifilm-instax.html)|```895 грн```|   |
[**Плівка Fujicolor Superia X-TRA 400**](https://www.amazon.com/Fujifilm-Superia-X-TRA-Negative-Exposures/dp/B07FP4L3PV/) [^2]|```25 $```|   |
[**Фотоапарат KODAK EKTAR H35**](https://www.amazon.com/KODAK-Reusable-Focus-Free-Lightweight-Included/dp/B0B1J67VPB/) [^2]|```44 $```|   |
[Чохол для KODAK EKTAR H35](https://www.amazon.com/Grapsa-Digital-Compatible-Storage-Organizer/dp/B0CJY8C2YF) [^2]|```17 $```|   |

# Інше


|   |   |   |
---|---:|---:|
[**Навушники Hator Hyperpunk 2 Wireless Tri-mode Black/Yellow**](https://rozetka.com.ua/ua/hator-hta-857/p396967395/) [^3]|```2799 грн```|   |

[^1]: Всиновити тваринку мені можна і без іграшки. Але деякі іграшки занадто милі :)
[^2]: Дарунок з-за кордону 
[^3]: Можна любій інші бездротові (не Bluetooth) навушники для ПК