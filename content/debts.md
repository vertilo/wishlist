---
title: "Борги"
date: 2024-08-21T01:30:44+02:00
draft: false
params:
    icon: meowth.png
---

<!-- # Хедер -->
\
{{< chart 90 300 >}}
{
    type: 'line',
    data: {
  labels: ["07.2024","08.2024","09.2024","10.2024"],
  datasets: [{
    label: 'Загальна сума боргів',
    data: [171360.68, 163360.68, 153099.97, 171251.97],
    fill: false,
    borderColor: 'rgb(213, 196, 161)',
    tension: 0.1
  }]
},
    options: {
        maintainAspectRatio: false,
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        },
    legend: {
        display: false
    }
    }
}
{{< /chart >}}
Загальна кількість боргів - **8**\
Вилпачено боргів - **0**
***
+ 21.08.2024 - Продаж <mark>планшету</mark>